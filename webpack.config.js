const path = require('path')
const cssExtract = require('mini-css-extract-plugin')
const nodeExternals = require('webpack-node-externals')
const { CheckerPlugin } = require('awesome-typescript-loader')

const mode = 'development'

const clientConfig = {
  mode,
  entry: {
    app: './client/src/index.ts'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './public')
  },
  devtool: 'none',
  module: {
    rules: [{
      test: /\.tsx?$/,
      include: path.resolve(__dirname, './client/src'),
      exclude: path.resolve(__dirname, 'node_modules'),
      use: [{
        loader: 'awesome-typescript-loader',
        options: {
          configFileName: './client/tsconfig.client.json'
        }
      }]
    }, {
      test: /\.scss$/,
      use: [
        mode === 'production' ? cssExtract.loader : 'style-loader',
        {
          loader: 'css-loader',
          options: {
            url: false
          }
        },
        'sass-loader'
      ]
    }]
  },
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx', '.jsx']
  },
  plugins: [
    new CheckerPlugin()
  ]
}

const serverConfig = {
  mode: mode,
  entry: {
    index: './server/src/index.ts'
  },
  output: {
    filename: '[name].js',
    path: __dirname
  },
  devtool: 'none',
  module: {
    rules: [{
      test: /\.ts$/,
      include: path.resolve(__dirname, './server/src'),
      exclude: path.resolve(__dirname, 'node_modules'),
      use: {
        loader: 'awesome-typescript-loader',
        options: {
          configFileName: './server/tsconfig.server.json'
        }
      }
    }]
  },
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx', '.jsx']
  },
  target: 'node',
  externals: [nodeExternals()],
  plugins: [
    new CheckerPlugin()
  ],
  node: {
    __dirname: false
  }
}

module.exports = [serverConfig, clientConfig]