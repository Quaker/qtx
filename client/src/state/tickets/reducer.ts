export interface Ticket {
  id: string
  status: string
  title: string
  content: string
  creatorId: string
  creatorEmail: string
  assignedId: string
  assignedEmail: string
}

export interface TicketsState {
  list: Ticket[]
}

const defaultState: TicketsState = {
  list: []
}

export function ticketsReducer(
  state = defaultState,
  action: any
): TicketsState {
  switch (action.type) {
    case 'TICKETS_CREATE': {
      return { ...state }
    }
    case 'TICKETS_READ': {
      const set = new Set()
      const tickets = action.payload.slice()
      action.payload.forEach((ticket: any) => set.add(ticket.id))
      state.list.forEach(ticket => {
        if (!set.has(ticket.id)) {
          tickets.push(ticket)
          set.add(ticket.id)
        }
      })
      return { ...state, list: tickets }
    }
    case 'TICKETS_ARCHIVE': {
      const archivedTicketId = action.payload
      const tickets = state.list.filter(
        ticket => ticket.id != archivedTicketId
      )
      return { ...state, list: tickets }
    }
  }
  return { ...state }
}
