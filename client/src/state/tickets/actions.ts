import { Ticket } from './reducer'

export function create(params: {
  id: string
  title: string
  content: string
  creatorId: string
  creatorEmail: string
}) {
  return {
    type: 'TICKETS_CREATE',
    payload: params
  }
}

export function read(tickets: Ticket[]) {
  return {
    type: 'TICKETS_READ',
    payload: tickets
  }
}

export function archive(ticketId: string) {
  return {
    type: 'TICKETS_ARCHIVE',
    payload: ticketId
  }
}

export function remove(ticketId: string) {
  return {
    type: 'TICKETS_REMOVE',
    payload: ticketId
  }
}
