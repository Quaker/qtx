export function login(email: string, token: string, isAdmin: boolean) {
  return {
    type: 'USER_LOGIN',
    payload: { email, token, isAdmin }
  }
}

export function logout() {
  return {
    type: 'USER_LOGOUT'
  }
}
