export interface UserState {
  email: string
  token: string
  isAdmin: boolean
}

const defaultState: UserState = {
  email: localStorage.getItem('email') || '',
  token: localStorage.getItem('token') || '',
  isAdmin: JSON.parse(localStorage.getItem('isAdmin') || 'false') || false
}

export function userReducer(
  state: UserState = defaultState,
  action: any
): UserState {
  switch (action.type) {
    case 'USER_LOGIN': {
      return {
        ...state,
        email: action.payload.email,
        token: action.payload.token,
        isAdmin: action.payload.isAdmin
      }
    }
    case 'USER_LOGOUT': {
      return { ...state, email: '', token: '' }
    }
  }
  return { ...state }
}
