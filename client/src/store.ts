import thunk from 'redux-thunk'
import { createStore, applyMiddleware, combineReducers, compose } from 'redux'
import { UserState, userReducer } from './state/users/reducer'
import { TicketsState, ticketsReducer } from './state/tickets/reducer'

export interface AppState {
  user: UserState
  tickets: TicketsState
}

const mainReducer = combineReducers<AppState>({
  user: userReducer,
  tickets: ticketsReducer
})

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(
  mainReducer,
  composeEnhancers(applyMiddleware(thunk))
) 