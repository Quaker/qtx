import * as React from 'react'
import * as TicketActions from '../../state/tickets/actions'
import { connect } from 'react-redux'
import axios from 'axios'
import { AppState } from '../../store'
import { match } from 'react-router'
import { Location, History } from 'history'

interface TicketEditProps {
  match: match<any>
  location: Location
  history: History
  email: string
  token: string
  isAdmin: boolean
  edit: (
    params: {
      id: string
      status: string
      title: string
      content: string
    }
  ) => void
  archive: (id: string) => void
  remove: (id: string) => void
}

interface TicketEditState {
  users: any[]
  creatorEmail: string
  status: string
  title: string
  content: string
  assigned: string
  invalidTicket: boolean
  done: boolean
  archived: boolean
  deleted: boolean
}

class _TicketEdit extends React.PureComponent<
  TicketEditProps,
  TicketEditState
> {
  state: TicketEditState = {
    users: [],
    creatorEmail: '',
    status: '',
    title: '',
    content: '',
    assigned: '',
    invalidTicket: false,
    done: false,
    archived: false,
    deleted: false
  }

  async componentDidMount() {
    try {
      const res = await axios.get(`/api/users`, {
        headers: {
          'Access-Control-Allow-Headers': 'x-access-token',
          'x-access-token': this.props.token
        }
      })
      const response = await axios.get(
        `/api/tickets/${this.props.match.params.id}`,
        {
          headers: {
            'Access-Control-Allow-Headers': 'x-access-token',
            'x-access-token': this.props.token
          }
        }
      )
      this.setState({
        users: res.data.users,
        creatorEmail: response.data.ticket.creatorEmail,
        status: response.data.ticket.status,
        title: response.data.ticket.title,
        content: response.data.ticket.content,
        assigned: response.data.ticket.assignedId
      })
    } catch (e) {
      this.setState({
        invalidTicket: true
      })
    }
  }

  onChange = (e: React.SyntheticEvent<HTMLFormElement>) => {
    const name = (e.target as any).name
    const value = (e.target as any).value

    this.setState({
      ...this.state,
      [name]: value
    })
  }

  onSubmit = async (e: React.SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault()

    try {
      const response = await axios.patch(
        `/api/tickets/${this.props.match.params.id}`,
        {
          status: this.state.status,
          title: this.state.title,
          content: this.state.content,
          assigned: this.state.assigned
        },
        {
          headers: {
            'Access-Control-Allow-Headers': 'x-access-token',
            'x-access-token': this.props.token
          }
        }
      )

      this.props.edit({
        id: response.data.id,
        status: response.data.status,
        title: response.data.title,
        content: response.data.content
      })

      this.setState({ done: true })
    } catch (e) {
      console.error(e)
    }
  }

  onArchive = async (e: React.SyntheticEvent<HTMLButtonElement>) => {
    e.preventDefault()

    try {
      await axios.delete(`/api/tickets/${this.props.match.params.id}`, {
        headers: {
          'Access-Control-Allow-Headers': 'x-access-token',
          'x-access-token': this.props.token
        }
      })
      this.setState({
        archived: true
      })
      this.props.archive(this.props.match.params.id)
    } catch (e) {
      // TODO
      console.log(e)
    }
  }

  onDelete = async (e: React.SyntheticEvent<HTMLButtonElement>) => {
    e.preventDefault()

    try {
      await axios.delete(`/api/tickets/${this.props.match.params.id}/delete`, {
        headers: {
          'Access-Control-Allow-Headers': 'x-access-token',
          'x-access-token': this.props.token
        }
      })
      this.setState({
        deleted: true
      })
      this.props.remove(this.props.match.params.id)
    } catch (e) {
      // TODO
      console.log(e)
    }
  }

  render() {
    if (this.state.done) {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">Ticket updated</div>
          </div>
        </div>
      )
    }

    if (this.state.archived) {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">Ticket archived</div>
          </div>
        </div>
      )
    }

    if (this.state.deleted) {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">Ticket deleted</div>
          </div>
        </div>
      )
    }

    const disabled =
      this.props.email !== this.state.creatorEmail && !this.props.isAdmin

    return (
      <div className="container">
        <br />

        <div className="container col-md-8">
          <form onChange={this.onChange} onSubmit={this.onSubmit}>
            <div className="form-group">
              <label htmlFor="ticket-edit-form-status">Status</label>
              <select
                name="status"
                id="ticket-edit-form-status"
                className="custom-select mb-3"
                value={this.state.status}
                disabled={disabled}
              >
                <option value="1">Pending</option>
                <option value="2">In Progress</option>
                <option value="3">Done</option>
              </select>
            </div>

            <div className="form-group">
              <label htmlFor="ticket-edit-form-title">Title</label>
              <input
                type="text"
                className={`form-control`}
                id="ticket-edit-form-title"
                placeholder="Title"
                name="title"
                value={this.state.title}
                disabled={disabled}
              />
            </div>

            <div className="form-group">
              <label htmlFor="ticket-edit-form-content">Content</label>
              <textarea
                className="form-control"
                id="ticket-edit-form-content"
                placeholder="Content"
                name="content"
                value={this.state.content}
                disabled={disabled}
              />
            </div>

            <div className="form-group">
              <label htmlFor="ticket-edit-form-assigned">Assignee</label>
              <select
                name="assigned"
                id="ticket-edit-form-assigned"
                className="custom-select mb-3"
                value={this.state.assigned}
                disabled={disabled}
              >
                <option value="">N/A</option>
                {this.state.users.map(user => (
                  <option value={user.id} key={user.id}>
                    {user.email}
                  </option>
                ))}
              </select>
            </div>

            <button
              type="submit"
              className="btn btn-primary"
              disabled={disabled}
            >
              Update
            </button>

            <button
              type="button"
              className="btn btn-danger ml-3"
              disabled={disabled}
              onClick={this.onArchive}
            >
              Archive
            </button>

            {this.props.isAdmin ? <button type="button" className="btn btn-danger ml-3" onClick={this.onDelete}>Delete</button> : <></>}
          </form>
        </div>
      </div>
    )
  }
}

export const TicketEdit = connect(
  (state: AppState) => ({
    email: state.user.email,
    token: state.user.token,
    isAdmin: state.user.isAdmin
  }),
  (dispatch: any) => ({
    edit: (params: {
      id: string
      title: string
      content: string
      creatorId: string
      creatorEmail: string
    }) => dispatch(TicketActions.create(params)),
    archive: (id: string) => dispatch(TicketActions.archive(id)),
    remove: (id: string) => dispatch(TicketActions.remove(id))
  })
)(_TicketEdit as any)
