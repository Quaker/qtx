import * as React from 'react'
import { Link, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import { AppState } from '../../store'

interface HeaderProps {
  email: string
  token: string
  isAdmin: boolean
}

class _Header extends React.Component<HeaderProps> {
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link to="/" className="navbar-brand">
          Qtx
        </Link>

        {this.props.token ? (
          <ul className="navbar-nav mr-auto">
            {this.props.isAdmin ? (
              <li className="nav-item">
                <NavLink
                  to="/admin"
                  className="nav-link"
                  activeClassName="active"
                >
                  Admin
                </NavLink>
              </li>
            ) : (
              <></>
            )}

            <li className="nav-item">
              <NavLink
                to="/tickets"
                className="nav-link"
                activeClassName="active"
              >
                Tickets
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="/logout"
                className="nav-link"
                activeClassName="active"
              >
                Logout
              </NavLink>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                Logged in as {this.props.email}
              </a>
            </li>
          </ul>
        ) : (
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <NavLink
                to="/register"
                className="nav-link"
                activeClassName="active"
              >
                Register
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                to="/login"
                className="nav-link"
                activeClassName="active"
              >
                Login
              </NavLink>
            </li>
          </ul>
        )}
      </nav>
    )
  }
}

export const Header = connect((state: AppState) => ({
  email: state.user.email,
  token: state.user.token,
  isAdmin: state.user.isAdmin
}))(_Header)
