import * as React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Home } from '../Home'
import { Login } from '../Login'
import { Register } from '../Register'
import { ActivateAccount } from '../ActivateAccount'
import { store } from '../../store'
import { Logout } from '../Logout'
import { Tickets } from '../Tickets'
import { TicketAdd } from '../TicketAdd'
import { TicketEdit } from '../TicketEdit'
import { Admin } from '../Admin';

export class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <>
            <Route path="/" component={Home} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
            <Route path="/activate" component={ActivateAccount} />
            <Route path="/logout" component={Logout} />
            <Switch>
              <Route exact path="/tickets" component={Tickets} />
              <Route path="/tickets/add" component={TicketAdd} />
              <Route path="/tickets/:id" component={TicketEdit} />
            </Switch>
            <Route path="/admin" component={Admin} />
          </>
        </Router>
      </Provider>
    )
  }
}
