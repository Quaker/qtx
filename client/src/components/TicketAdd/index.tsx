import * as React from 'react'
import * as TicketActions from '../../state/tickets/actions'
import { connect } from 'react-redux'
import axios from 'axios'
import { AppState } from '../../store';

interface TicketAddProps {
  token: string
  create: (params: {
    id: string
    title: string
    content: string
    creatorId: string
    creatorEmail: string
  }) => void
}

interface TicketAddState {
  title: string
  content: string
  done: boolean
}

class _TicketAdd extends React.PureComponent<TicketAddProps, TicketAddState> {
  state: TicketAddState = {
    title: '',
    content: '',
    done: false
  }

  onChange = (e: React.SyntheticEvent<HTMLFormElement>) => {
    const name = (e.target as any).name
    const value = (e.target as any).value

    this.setState({
      ...this.state,
      [name]: value
    })
  }

  onSubmit = async (e: React.SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault()

    try {
      const response = await axios.post('/api/tickets', {
        title: this.state.title,
        content: this.state.content
      }, {
        headers: {
          'Access-Control-Allow-Headers': 'x-access-token',
          'x-access-token': this.props.token
        }
      })

      this.props.create({
        id: response.data.id,
        title: response.data.title,
        content: response.data.content,
        creatorId: response.data.creatorId,
        creatorEmail: response.data.creatorEmail
      })

      this.setState({ done: true })

    } catch (e) {
      console.error(e)
    }
  }

  render() {
    if (this.state.done) {
      return <div className="container text-center">
        <br />
        <div className="row justify-content-center">
          <div className="col-lg-6">Ticket created</div>
        </div>
      </div>
    }

    return (
      <div className="container">
        <br />

        <div className="container col-md-8">
          <form onChange={this.onChange} onSubmit={this.onSubmit}>
            <div className="form-group">
              <label htmlFor="ticket-create-form-title">Title</label>
              <input
                type="text"
                className={`form-control`}
                id="ticket-create-form-title"
                placeholder="Title"
                name="title"
                value={this.state.title}
              />
            </div>

            <div className="form-group">
              <label htmlFor="login-form-password">Content</label>
              <textarea
                className="form-control"
                id="login-form-password"
                placeholder="Content"
                name="content"
                value={this.state.content}
              />
            </div>

            <button type="submit" className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    )
  }
}

export const TicketAdd = connect(
  (state: AppState) => ({
    token: state.user.token
  }),
  (dispatch: any) => ({
    create: (params: {
      id: string
      title: string
      content: string
      creatorId: string
      creatorEmail: string
    }) =>
      dispatch(TicketActions.create(params))
  })
)(_TicketAdd)
