import * as React from 'react'
import axios from 'axios'
import { match } from 'react-router'
import { Location, History } from 'history'
import * as UserActions from '../../state/users/actions'
import { connect } from 'react-redux'

interface LoginProps {
  match: match<any>
  location: Location
  history: History
  login: (email: string, token: string, isAdmin: boolean) => void
}

interface LoginState {
  email: {
    value: string
    validated: boolean
    error: string
  }
  password: {
    value: string
    validated: boolean
    error: string
  }
}

class _Login extends React.PureComponent<LoginProps, LoginState> {
  state = {
    email: {
      value: '',
      validated: false,
      error: ''
    },
    password: {
      value: '',
      validated: false,
      error: ''
    }
  }

  onChange = (e: React.SyntheticEvent<HTMLFormElement>) => {
    const name = (e.target as HTMLInputElement).name
    const value = (e.target as HTMLInputElement).value
    this.setState({
      [name]: {
        value,
        validated: false,
        error: ''
      }
    } as any)
  }

  onSubmit = async (e: React.SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault()

    if (!this.state.email) {
      this.setState({
        email: {
          ...(this.state as any).email,
          error: 'Email required',
          validated: true
        }
      })
      return
    }

    if (!this.state.password) {
      this.setState({
        password: {
          ...(this.state as any).email,
          error: 'Password required',
          validated: true
        }
      })
      return
    }

    try {
      const response = await axios.post('/api/users/authenticate', {
        email: this.state.email.value,
        password: this.state.password.value
      })

      this.setState({
        email: {
          ...this.state.email,
          validated: true
        },
        password: {
          ...this.state.password,
          validated: true
        }
      })

      const email = this.state.email.value
      const token = response.data.token
      const isAdmin = response.data.isAdmin

      localStorage.setItem('token', token)
      localStorage.setItem('email', email)
      localStorage.setItem('isAdmin', isAdmin)

      this.props.login(email, token, isAdmin)

      this.props.history.push('/')
    } catch (e) {
      const response = e.response
      this.setState({
        email: {
          ...this.state.email,
          error: response.data.emailErrors.join(''),
          validated: true
        },
        password: {
          ...this.state.password,
          error: response.data.passwordErrors.join(''),
          validated: true
        }
      })
    }
  }

  private getValidClassName(field: string) {
    if ((this.state as any)[field].validated) {
      if ((this.state as any)[field].error.length) {
        return 'is-invalid'
      } else {
        return 'is-valid'
      }
    } else {
      return ''
    }
  }

  render() {
    return (
      <div className="container">
        <br />
        <div className="container col-md-8">
          <form onChange={this.onChange} onSubmit={this.onSubmit}>
            <div className="form-group">
              <label htmlFor="login-form-email">Email address</label>
              <input
                type="email"
                className={`form-control ${this.getValidClassName('email')}`}
                id="login-form-email"
                placeholder="Email"
                name="email"
                value={this.state.email.value}
              />
              <div className="invalid-feedback">{this.state.email.error}</div>
            </div>

            <div className="form-group">
              <label htmlFor="login-form-password">Password</label>
              <input
                type="password"
                className={`form-control ${this.getValidClassName('password')}`}
                id="login-form-password"
                placeholder="Password"
                name="password"
                value={this.state.password.value}
              />
              <div className="invalid-feedback">
                {this.state.password.error}
              </div>
            </div>

            <button type="submit" className="btn btn-primary">
              Login
            </button>
          </form>
        </div>
      </div>
    )
  }
}

export const Login = connect(
  () => ({}),
  (dispatch: any) => ({
    login: (email: string, token: string, isAdmin: boolean) =>
      dispatch(UserActions.login(email, token, isAdmin))
  })
)(_Login)
