import * as React from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { AppState } from '../../store'

interface AdminProps {
  email: string
  token: string
  isAdmin: boolean
}

interface AdminState {
  users: any[]
}

class _Admin extends React.Component<AdminProps, AdminState> {
  state: AdminState = {
    users: []
  }

  async componentDidMount() {
    try {
      const response = await axios.get('/api/users', {
        headers: {
          'Access-Control-Allow-Headers': 'x-access-token',
          'x-access-token': this.props.token
        }
      })
      this.setState({
        users: response.data.users
      })
    } catch (e) {
      console.log(e)
    }
  }

  onActivate = async (e: React.SyntheticEvent<HTMLButtonElement>, id: string) => {
    e.preventDefault()

    try {
      await axios.post(`/api/users/${id}/activate`, {}, {
        headers: {
          'Access-Control-Allow-Headers': 'x-access-token',
          'x-access-token': this.props.token
        }
      })
      this.setState({
        users: this.state.users.slice().map((user: any) => {
          if (user.id == id) {
            user.activated = 1
          }

          return user
        })
      })
    } catch (e) {
      console.log(e)
    }
  }

  onRemove = async (e: React.SyntheticEvent<HTMLButtonElement>, id: string) => {
    e.preventDefault()

    try {
      await axios.delete(`/api/users/${id}`, {
        headers: {
          'Access-Control-Allow-Headers': 'x-access-token',
          'x-access-token': this.props.token
        }
      })
      this.setState({
        users: this.state.users.filter((user: any) => user.id != id)
      })
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    return (
      <div className="container mt-3">
        <div className="row justify-content-center">
          <div className="col-lg-10">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Email</th>
                  <th />
                  <th />
                </tr>
              </thead>
              <tbody>
                {this.state.users.map((user: any) => (
                  <tr key={user.id}>
                    <td>{user.id}</td>
                    <td>{user.email}</td>
                    <td>
                      {user.activated ? (
                        <></>
                      ) : (
                        <button
                          type="button"
                          className="btn btn-primary mr-2"
                          onClick={(e: any) => this.onActivate(e, user.id)}
                        >
                          Activate
                        </button>
                      )}
                    </td>
                    <td>
                      <button
                        type="button"
                        className="btn btn-danger"
                        onClick={(e: any) => this.onRemove(e, user.id)}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

export const Admin = connect((state: AppState) => ({
  email: state.user.email,
  token: state.user.token,
  isAdmin: state.user.isAdmin
}))(_Admin)
