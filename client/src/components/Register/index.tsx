import * as React from 'react'
import axios from 'axios'

interface RegisterState {
  email: {
    value: string
    validated: boolean
    error: string
  }
  password: {
    value: string
    validated: boolean
    error: string
  }
  done: boolean
}

export class Register extends React.Component<{}, RegisterState> {
  state = {
    email: {
      value: '',
      validated: false,
      error: ''
    },
    password: {
      value: '',
      validated: false,
      error: ''
    },
    done: false
  }

  onChange = (e: React.SyntheticEvent<HTMLFormElement>) => {
    const name = (e.target as HTMLInputElement).name
    const value = (e.target as HTMLInputElement).value
    this.setState({
      [name]: {
        ...(this.state as any)[name],
        value,
        validated: false
      }
    } as any)
  }

  onSubmit = async (e: React.SyntheticEvent<HTMLFormElement>) => {
    e.preventDefault()

    if (!this.state.email) {
      this.setState({
        email: {
          ...(this.state as any).email,
          error: 'Email required',
          validated: true
        }
      })
      return
    }

    if (!this.state.password) {
      this.setState({
        password: {
          ...(this.state as any).password,
          error: 'Password required',
          validated: true
        }
      })
      return
    }

    try {
      await axios.post('/api/users', {
        email: this.state.email.value,
        password: this.state.password.value
      })

      this.setState({
        email: {
          ...this.state.email,
          validated: true
        },
        password: {
          ...this.state.password,
          validated: true
        },
        done: true
      })
    } catch (e) {
      const response = e.response
      this.setState({
        email: {
          ...this.state.email,
          error: response.data.emailErrors.join(''),
          validated: true
        },
        password: {
          ...this.state.password,
          error: response.data.passwordErrors.join(''),
          validated: true
        }
      })
    }
  }

  private getValidClassName(field: string) {
    if ((this.state as any)[field].validated) {
      if ((this.state as any)[field].error.length) {
        return 'is-invalid'
      } else {
        return 'is-valid'
      }
    } else {
      return ''
    }
  }

  render() {
    if (this.state.done) {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">
              Registration successful
              <br />
              Account activation link was sent to your email
            </div>
          </div>
        </div>
      )
    }

    return (
      <div className="container">
        <br />
        <div className="container col-md-8">
          <form onChange={this.onChange} onSubmit={this.onSubmit}>
            <div className="form-group">
              <label htmlFor="register-form-email">Email address</label>
              <input
                type="email"
                className={`form-control ${this.getValidClassName('email')}`}
                id="register-form-email"
                placeholder="Email"
                name="email"
                value={this.state.email.value}
              />
              <div className="invalid-feedback">{this.state.email.error}</div>
            </div>

            <div className="form-group">
              <label htmlFor="register-form-password">Password</label>
              <input
                type="password"
                className={`form-control ${this.getValidClassName('password')}`}
                id="register-form-password"
                placeholder="Password"
                name="password"
                value={this.state.password.value}
              />
              <div className="invalid-feedback">
                {this.state.password.error}
              </div>
            </div>

            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>
    )
  }
}
