import * as React from 'react'
import qs from 'qs'
import axios from 'axios'
import { match } from 'react-router'
import { Location, History } from 'history'
import { Link } from 'react-router-dom'

interface ActivateAccountProps {
  match: match<any>
  location: Location
  history: History
}

interface ActivateAccountState {
  isValidating: boolean
  isValid: boolean
}

export class ActivateAccount extends React.PureComponent<ActivateAccountProps> {
  state: ActivateAccountState = {
    isValidating: true,
    isValid: false
  }

  async componentDidMount() {
    const query = this.props.location.search
    const params = qs.parse(query, { ignoreQueryPrefix: true })
    if (!params.code) {
      return
    }

    try {
      await axios.post('/api/users/activate', {
        code: params.code
      })
      this.setState({ isValidating: false, isValid: true })
    } catch (e) {
      this.setState({ isValidating: false, isValid: false })
    }
  }

  render() {
    if (this.state.isValidating) {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">Activation in progress</div>
          </div>
        </div>
      )
    } else if (this.state.isValid) {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">Successfully activated account</div>
          </div>
          <div className="row justify-content-center">
            <div className="col-lg-6">
              Go to <Link to="/login">Login</Link>
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">Invalid activation code</div>
          </div>
        </div>
      )
    }
  }
}
