import * as React from 'react'
import * as UserActions from '../../state/users/actions'
import { connect } from 'react-redux'
import { AppState } from '../../store'

interface LogoutProps {
  email: string
  token: string
  logout: () => void
}

class _Logout extends React.Component<LogoutProps> {
  componentDidMount() {
    localStorage.removeItem('token')
    localStorage.removeItem('email')
    this.props.logout()
  }

  render() {
    if (this.props.email && this.props.token) {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">Logging out...</div>
          </div>
        </div>
      )
    } else {
      return (
        <div className="container text-center">
          <br />
          <div className="row justify-content-center">
            <div className="col-lg-6">Logged out</div>
          </div>
        </div>
      )
    }
  }
}

export const Logout = connect(
  (state: AppState) => ({
    email: state.user.email,
    token: state.user.token
  }),
  (dispatch: any) => ({
    logout: () => dispatch(UserActions.logout())
  })
)(_Logout)
