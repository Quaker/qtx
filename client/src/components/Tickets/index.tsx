import * as React from 'react'
import { connect } from 'react-redux'
import { AppState } from '../../store'
import { Ticket } from '../../state/tickets/reducer'
import { Link } from 'react-router-dom'
import * as TicketActions from '../../state/tickets/actions'
import axios from 'axios'

interface TicketsProps {
  token: string
  tickets: Ticket[]
  read: (ticket: Ticket[]) => void
}

class _Tickets extends React.Component<TicketsProps> {
  async componentDidMount() {
    try {
      const response = await axios.get('/api/tickets', {
        headers: {
          'Access-Control-Allow-Headers': 'x-access-token',
          'x-access-token': this.props.token
        }
      })
      this.props.read(response.data.tickets)
    } catch (e) {
      console.error(e)
    }
  }

  getStatusName(status: string) {
    switch (parseInt(status)) {
      case 1: {
        return 'Pending'
      }
      case 2: {
        return 'In Progress'
      }
      case 3: {
        return 'Done'
      }
    }
  }

  render() {
    return (
      <div className="container text-center">
        <br />

        <div className="row">
          <div className="col-lg-12">
            <button className="btn btn-light">
              <Link to="/tickets/add">Add new</Link>
            </button>
          </div>
        </div>

        <br />

        <div className="row justify-content-center">
          <div className="col-lg-10">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Title</th>
                  <th>Content</th>
                  <th>Status</th>
                  <th>Creator</th>
                  <th>Assignee</th>
                </tr>
              </thead>
              <tbody>
                {this.props.tickets.map(ticket => (
                  <tr key={ticket.id}>
                    <td>{ticket.id}</td>
                    <td>
                      <Link to={`/tickets/${ticket.id}`}>{ticket.title}</Link>
                    </td>
                    <td>{ticket.content}</td>
                    <td>{this.getStatusName(ticket.status)}</td>
                    <td>{ticket.creatorEmail}</td>
                    <td>{ticket.assignedEmail}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}

export const Tickets = connect(
  (state: AppState) => ({
    token: state.user.token,
    tickets: state.tickets.list
  }),
  (dispatch: any) => ({
    read: (tickets: Ticket[]) => dispatch(TicketActions.read(tickets))
  })
)(_Tickets)
