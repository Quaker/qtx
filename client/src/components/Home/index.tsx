import * as React from 'react'
import { Header } from '../Header'

export class Home extends React.PureComponent {
  render() {
    return (
      <div className="container mt-3">
        <Header />
      </div>
    )
  }
}
