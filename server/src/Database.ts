import * as sqlite from 'sqlite'

export class Database {
  static async create(filename: string): Promise<Database | null> {
    const db = await Database.createDatabase(filename)
    if (!db) {
      return null
    }

    if (!(await Database.migrate(db))) {
      return null
    }

    return new Database(db)
  }

  static async createDatabase(
    filename: string
  ): Promise<sqlite.Database | null> {
    try {
      return await sqlite.open(filename, { verbose: true })
    } catch (e) {
      console.error('Failed to open database')
      console.error(e)
      return null
    }
  }

  static async migrate(db: sqlite.Database) {
    try {
      await db.migrate({})
    } catch (e) {
      console.error('Failed to execute database migrations')
      console.error(e)
      return false
    }

    return true
  }

  private db: sqlite.Database

  private constructor(db: sqlite.Database) {
    this.db = db
  }

  getDb() {
    return this.db
  }

  async findUserByEmail(
    email: string
  ): Promise<{ [name: string]: string } | null> {
    const users = await this.db.all(
      'SELECT * FROM users WHERE email = ? LIMIT 1',
      email
    )

    if (users.length) {
      return users[0]
    } else {
      return null
    }
  }

  async readUsers() {
    return await this.db.all('SELECT * FROM users')
  }

  async findUserById(id: string) {
    const statement = await this.db.prepare(
      'SELECT * FROM users WHERE id = ? LIMIT 1',
      id
    )
    return statement.get()
  }

  async insertUser(params: {
    email: string
    password: string
    activationCode: string
  }) {
    const statement = await this.db.prepare(
      `INSERT INTO users
      (email, password, activation_code, activated)
      VALUES
      (?, ?, ?, ?)`,
      params.email,
      params.password,
      params.activationCode,
      0
    )

    await statement.run()
  }

  async findUserByActivationCode(code: string) {
    return this.db.all(
      'SELECT * FROM users WHERE activation_code = ? AND activated = 0 LIMIT 1',
      code
    )
  }

  async activateUser(id: string) {
    const statement = await this.db.prepare(
      'UPDATE users SET activated = 1 WHERE id = ?',
      id
    )
    await statement.run()
  }

  async deleteUser(id: string) {
    const statement = await this.db.prepare(
      'DELETE FROM users WHERE id = ?',
      id
    )
    await statement.run()
  }

  async createTicket(params: {
    title: string
    content: string
    creator: number
  }) {
    const statement = await this.db.prepare(
      `INSERT INTO tickets (status, title, content, creator)
      VALUES (?, ?, ?, ?)`,
      1,
      params.title,
      params.content,
      params.creator
    )

    await statement.run()

    return statement.lastID
  }

  async readTickets() {
    return await this.db.all(
      `SELECT
        t.id as id,
        t.status as status,
        t.title as title,
        t.content as content,
        u.id as creatorId,
        u.email as creatorEmail,
        t.assigned as assignedId,
        u2.email as assignedEmail
      FROM tickets AS t
      JOIN users AS u ON t.creator = u.id
      LEFT JOIN users AS u2 ON t.assigned = u2.id
      WHERE archived = 0`
    )
  }

  async readTicket(id: string) {
    const statement = await this.db.prepare(
      `SELECT
        t.id as id,
        t.status as status,
        t.title as title,
        t.content as content,
        u.id as creatorId,
        u.email as creatorEmail,
        t.assigned as assignedId,
        u2.email as assignedEmail
      FROM tickets AS t
      JOIN users AS u ON t.creator = u.id
      LEFT JOIN users AS u2 ON t.assigned = u2.id
      WHERE t.id = ? AND archived = 0
      LIMIT 1`,
      id
    )
    return await statement.get()
  }

  async updateTicket(
    id: string,
    params: {
      status: string
      title: string
      content: string
      assigned: string
    }
  ) {
    const statement = await this.db.prepare(
      `UPDATE tickets
      SET status = ?, title = ?, content = ?, assigned = ?
      WHERE id = ?`,
      params.status,
      params.title,
      params.content,
      params.assigned,
      id
    )
    await statement.run()
  }

  async updatedAssigned(ticketId: string, assignedId: string) {
    const statement = await this.db.prepare(
      `UPDATE tickets
      SET assigned = ?
      WHERE id = ?`,
      assignedId,
      ticketId
    )
    await statement.run()
  }

  async archiveTicket(id: string) {
    const statement = await this.db.prepare(
      `UPDATE tickets SET archived = 1 WHERE id = ?`,
      id
    )
    return statement.run()
  }

  async deleteTicket(id: string) {
    const statement = await this.db.prepare(
      `DELETE FROM tickets WHERE id = ?`,
      id
    )
    return statement.run()
  }
}
