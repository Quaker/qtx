import * as express from 'express'
import * as path from 'path'
import * as bodyParser from 'body-parser'
import { config } from './config'
import { Database } from './Database'
import * as Controllers from './controllers'
import { Mailer } from './Mailer';

const asyncErrorHandler = (
  f: (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
    ...args: any[]
  ) => Promise<any>
) => {
  return (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
    ...args: any[]
  ) => {
    return f(req, res, next, ...args).catch(next)
  }
}

export class App {
  static async start() {
    const db = await Database.create(config.database.filename)
    const mailer = new Mailer(config)

    const app = express()

    app.set('db', db)
    app.set('mailer', mailer)

    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(express.static('./public'))

    // in combination with asyncErrorHandler will return 500 instead of crashing
    app.use(
      (
        err: any,
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => {
        console.error(err.stack)
        res.status(500).send({ message: 'Server error!' })
      }
    )

    app.post('/api/users', asyncErrorHandler(Controllers.users.register))
    app.get('/api/users', asyncErrorHandler(Controllers.users.read))
    app.post('/api/users/authenticate', asyncErrorHandler(Controllers.users.login))
    app.post('/api/users/activate', asyncErrorHandler(Controllers.users.activate))
    app.post('/api/users/:id/activate', asyncErrorHandler(Controllers.users.activateByAdmin))
    app.delete('/api/users/:id', asyncErrorHandler(Controllers.users.remove))
    app.post('/api/tickets', asyncErrorHandler(Controllers.tickets.create))
    app.get('/api/tickets', asyncErrorHandler(Controllers.tickets.read))
    app.get('/api/tickets/:id', asyncErrorHandler(Controllers.tickets.readOne))
    app.patch('/api/tickets/:id', asyncErrorHandler(Controllers.tickets.update))
    app.delete('/api/tickets/:id/delete', asyncErrorHandler(Controllers.tickets.remove))
    app.delete('/api/tickets/:id', asyncErrorHandler(Controllers.tickets.archive))

    app.get('*', (_, res) => {
      res.sendFile(path.resolve(`${__dirname}/public/index.html`))
    })
    app.listen(config.http.port, () => {
      console.log(`Server started listening on port ${config.http.port}`)
    })
  }

  private constructor() {}
}
