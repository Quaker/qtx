import * as express from 'express'
import { verify } from 'jsonwebtoken'
import { config } from '../../config'
import { Database } from '../../Database'

export async function update(req: express.Request, res: express.Response) {
  const token = req.header('x-access-token')
  if (!token) {
    // TODO: no token
    res.status(400).send({
      error: 'Access token missing'
    })
    return
  }

  const tokenData: any = verify(token, config.jwt.secret)
  if (!tokenData) {
    res.status(400).send({
      error: 'Invalid access token'
    })
    return
  }

  const id = req.params.id
  const status = req.body.status
  const title = req.body.title
  const content = req.body.content
  const assigned = req.body.assigned
  if (!status || !title || !content || !assigned) {
    // TODO: proper validation
    // console.log('Invalid params')
    res.status(400).send({})
    return
  }

  const db: Database = req.app.get('db')

  const ticket = await db.readTicket(id)
  if (!ticket) {
    // TODO: ticket with id not found
    // console.log(`Ticket not found with id = ${id}`)
    res.status(400).send({})
    return
  }

  if (ticket.creatorId !== tokenData.id && !tokenData.isAdmin) {
    // TODO: not your ticket
    // console.log('Not your ticket')
    res.status(400).send({})
    return
  }

  await db.updateTicket(id, { status, title, content, assigned })

  res.send({})
}
