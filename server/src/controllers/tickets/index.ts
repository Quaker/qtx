import { create } from './create'
import { read } from './read'
import { readOne } from './readOne'
import { update } from './update'
import { archive } from './archive'
import { remove } from './remove'

export { create, read, readOne, update, archive, remove }
