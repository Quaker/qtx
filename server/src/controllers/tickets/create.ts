import * as express from 'express'
import { verify } from 'jsonwebtoken'
import { config } from '../../config'
import { Database } from '../../Database'

export async function create(req: express.Request, res: express.Response) {
  const token = req.header('x-access-token')
  if (!token) {
    // TODO: no token
    res.status(400).send({
      error: 'Access token missing'
    })
    return
  }

  const tokenData: any = verify(token, config.jwt.secret)
  if (!tokenData) {
    res.status(400).send({
      error: 'Invalid access token'
    })
    return
  }

  const title = req.body.title
  const content = req.body.content
  if (!title || !content) {
    res.status(400).send({})
    return
  }

  const db: Database = req.app.get('db')

  const id = await db.createTicket({
    title,
    content,
    creator: tokenData.id
  })

  res.send({
    id,
    title,
    content,
    creatorId: tokenData.id,
    creatorEmail: tokenData.email
  })
}
