import * as express from 'express'
import { verify } from 'jsonwebtoken'
import { config } from '../../config'
import { Database } from '../../Database'

export async function archive(req: express.Request, res: express.Response) {
  const token = req.header('x-access-token')
  if (!token) {
    // TODO: no token
    res.status(400).send({
      error: 'Access token missing'
    })
    return
  }

  const tokenData: any = verify(token, config.jwt.secret)
  if (!tokenData) {
    res.status(400).send({
      error: 'Invalid access token'
    })
    return
  }

  const db: Database = req.app.get('db')

  const id = req.params.id
  const ticket = await db.readTicket(id)
  if (!ticket) {
    // TODO: ticket with id not found
    // console.log(`Ticket not found with id = ${id}`)
    res.status(400).send({})
    return
  }

  if (ticket.creatorId !== tokenData.id) {
    // TODO: not your ticket
    // console.log('Not your ticket')
    res.status(400).send({})
    return
  }

  await db.archiveTicket(id)

  res.send({})
}
