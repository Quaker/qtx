import * as express from 'express'
import { Database } from '../../Database'

export async function activate(req: express.Request, res: express.Response) {
  const code = req.body.code
  if (!code) {
    res.status(400).send({})
  }

  const db: Database = req.app.get('db')
  const users = await db.findUserByActivationCode(code)
  if (!users.length) {
    res.status(400).send({})
    return
  }

  await db.activateUser(users[0].id)
  
  res.send({})
}
