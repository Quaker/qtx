import { login } from './login'
import { register } from './register'
import { activate } from './activate'
import { read } from './read'
import { activateByAdmin } from './activateByAdmin'
import { remove } from './remove'

export { login, register, activate, read, activateByAdmin, remove }
