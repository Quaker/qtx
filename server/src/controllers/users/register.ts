import * as express from 'express'
import { hash } from 'bcrypt'
import { Database } from '../../Database'
import { Mailer } from '../../Mailer'

const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

function randomString(length: number) {
  let text = ''
  const possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }

  return text
}

export async function register(req: express.Request, res: express.Response) {
  // TODO: use validation library
  const email: string = req.body.email
  const password: string = req.body.password

  let hasErrors = false
  const errors: {
    emailErrors: string[]
    passwordErrors: string[]
  } = {
    emailErrors: [],
    passwordErrors: []
  }

  if (!email) {
    errors.emailErrors.push('Email is required')
    hasErrors = true
  } else if (!email.match(emailRegex)) {
    errors.emailErrors.push('Invalid email format')
    hasErrors = true
  }

  if (!password) {
    errors.passwordErrors.push('Password is required')
    hasErrors = true
  } else if (password.length < 4) {
    errors.passwordErrors.push('Password must have at least 4 characters')
    hasErrors = true
  }

  if (hasErrors) {
    res.status(400).send(errors)
    return
  }

  const db: Database = req.app.get('db')
  const existingUser = await db.findUserByEmail(email)
  if (existingUser) {
    errors.emailErrors.push('Already registered with that email')
    res.status(400).send(errors)
    return
  }

  const hashedPassword = await hash(password, 10)
  const activationCode = randomString(10)
  await db.insertUser({
    email,
    password: hashedPassword,
    activationCode
  })

  const mailer: Mailer = req.app.get('mailer')
  const mailStatus = await mailer.sendActivationEmail(email, activationCode)
  if (mailStatus) {
    console.error(mailStatus)
    errors.emailErrors.push('Failed to send account activation email')
    res.status(500).send(errors)
    return
  } else {
    console.log(`Successfully sent account activation email to ${email}`)
  }

  res.send({})
}
