import * as express from 'express'
import { verify } from 'jsonwebtoken'
import { config } from '../../config'
import { Database } from '../../Database'

export async function remove(req: express.Request, res: express.Response) {
  const token = req.header('x-access-token')
  if (!token) {
    // TODO: no token
    res.status(400).send({
      error: 'Access token missing'
    })
    return
  }

  const tokenData: any = verify(token, config.jwt.secret)
  if (!tokenData) {
    res.status(400).send({
      error: 'Invalid access token'
    })
    return
  }

  const db: Database = req.app.get('db')

  const id = req.params.id
  const user = await db.findUserById(id)
  if (!user) {
    // TODO: user with id not found
    // console.log(`User not found with id = ${id}`)
    res.status(400).send({})
    return
  }

  if (!tokenData.isAdmin) {
    // TODO: not an admin
    // console.log('Not an admin')
    res.status(400).send({})
    return
  }

  await db.deleteUser(id)

  res.send({})
}
