import * as express from 'express'
import { Database } from '../../Database'
import { config } from '../../config'
import { verify } from 'jsonwebtoken'

export async function activateByAdmin(
  req: express.Request,
  res: express.Response
) {
  const token = req.header('x-access-token')
  if (!token) {
    // TODO: no token
    res.status(400).send({
      error: 'Access token missing'
    })
    return
  }

  const tokenData: any = verify(token, config.jwt.secret)
  if (!tokenData) {
    res.status(400).send({
      error: 'Invalid access token'
    })
    return
  }

  const id = req.params.id
  const db: Database = req.app.get('db')
  await db.activateUser(id)

  res.send({})
}
