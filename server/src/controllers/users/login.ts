import * as express from 'express'
import * as jwt from 'jsonwebtoken'
import { compare } from 'bcrypt'
import { Database } from '../../Database'
import { config } from '../../config'

const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

export async function login(req: express.Request, res: express.Response) {
  // TODO: use validation library
  const email: string = req.body.email
  const password: string = req.body.password

  let hasErrors = false
  const errors: {
    emailErrors: string[]
    passwordErrors: string[]
  } = {
    emailErrors: [],
    passwordErrors: []
  }

  if (!email) {
    errors.emailErrors.push('Email is required')
    hasErrors = true
  } else if (!email.match(emailRegex)) {
    errors.emailErrors.push('Invalid email format')
    hasErrors = true
  }

  if (!password) {
    errors.passwordErrors.push('Password is required')
    hasErrors = true
  } else if (password.length < 4) {
    errors.passwordErrors.push('Password must have at least 4 characters')
    hasErrors = true
  }

  if (hasErrors) {
    res.status(400).send(errors)
    return
  }

  const db: Database = req.app.get('db')
  const user = await db.findUserByEmail(email)
  if (!user) {
    errors.emailErrors.push('Account with given email not found')
    res.status(400).send(errors)
    return
  }

  if (!user.activated) {
    errors.emailErrors.push('Account not activated')
    res.status(400).send(errors)
    return
  }

  const isCorrectPassword = await compare(password, user.password)
  if (!isCorrectPassword) {
    errors.passwordErrors.push('Password incorrect')
    res.status(400).send(errors)
    return
  }

  const isAdmin = !!user.is_admin

  const token = jwt.sign(
    {
      id: user.id,
      email: user.email,
      isAdmin
    },
    config.jwt.secret,
    {
      expiresIn: 86400 // 24 hours
    }
  )

  res.send({ email, token, isAdmin })
}
