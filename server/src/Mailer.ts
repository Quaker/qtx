import * as nodemailer from 'nodemailer'

export class Mailer {
  private transporter: nodemailer.Transporter
  private config: any

  constructor(config: any) {
    this.config = config
    this.transporter = nodemailer.createTransport(config.mail)
  }

  async sendActivationEmail(to: string, code: string): Promise<string | null> {
    const config = this.config

    const mailOptions = {
      to,
      from: `qtx <${config.mail.auth.user}>`,
      subject: 'QTX Account Activation Email',
      text: `Account activation link: http://localhost:${
        config.http.port
      }/activate=${code}`,
      html: `Account activation link: <a href="http://localhost:${
        config.http.port
      }/activate?code=${code}">activate</a>`
    }

    try {
      await this.transporter.sendMail(mailOptions)
      return null
    } catch (e) {
      console.error(e)
      return 'Failed to send account activation email'
    }
  }
}
