-- Up
CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  email TEXT UNIQUE,
  password TEXT,
  activation_code TEXT UNIQUE,
  activated INTEGER,
  is_admin INTEGER
);

CREATE TABLE tickets (
  id INTEGER PRIMARY KEY,
  status INTEGER DEFAULT 1,
  title TEXT NOT NULL,
  content TEXT NOT NULL DEFAULT '',
  creator INTEGER NOT NULL,
  assigned INTEGER NULL DEFAULT NULL,
  archived INTEGER NOT NULL DEFAULT 0,

  CONSTRAINT tickets_fk_userid FOREIGN KEY (creator)
    REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE,
  
  CONSTRAINT tickets_fk_assignedid FOREIGN KEY (assigned)
    REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE INDEX idx_users_email ON users (email);
CREATE INDEX idx_users_activation_code ON users (activation_code);
CREATE INDEX idx_tickets_creator ON tickets (creator);
CREATE INDEX idx_tickets_assigned ON tickets (assigned);
CREATE INDEX idx_archived ON tickets (archived);

INSERT INTO users (email, password, activation_code, activated, is_admin)
  VALUES ('admin@admin.com', '$2b$10$AIUprKX4/.gqp.yFUXGEDeLEqNSo.zQNXeB6yRH29mHGO0fPC.e76', 'admin', 1, 1);

-- Down
DROP TABLE tickets;
DROP TABLE users;