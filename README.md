## Instructions

1. `git clone` this repo
2. `npm install`
3. make `client/src/config.ts` file (see sample file see sample info)
4. make `server/config.js` file  (see sample config for more info)
5. `npm run build`
6. `npm start`

Sample user:
  - user: admin@admin.com
  - pw: admin